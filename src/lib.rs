use std::process::{Command, Stdio, Child};

use std::io::{self, Write};

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum AudioFormat {
    U8,
    S8,
    U16Le,
    U16Be,
    S16Le,
    S16Be,
    U24Le,
    U24Be,
    S24Le,
    S24Be,
    U32Le,
    U32Be,
    S32Le,
    S32Be,
    FloatLe,
    FloatBe,
    DoubleLe,
    DoubleBe,
    U16,
    S16,
    U24,
    S24,
    U32,
    S32,
    Float,
    Double,
}

impl std::fmt::Display for AudioFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            default @ _ => write!(f, "{}", format!("{:?}", default).to_ascii_lowercase()),
        }
    }
}

pub struct AudioOut {
   child: Child 
}

impl AudioOut {
    pub fn open(af: AudioFormat, rate: i32, channels: i32) -> io::Result<AudioOut> {
        // what else did you expect?
        let mut cmd = Command::new("mpv");
        cmd.arg("--demuxer=rawaudio")
            .arg(format!("--demuxer-rawaudio-channels={}", channels))
            .arg(format!("--demuxer-rawaudio-format={}", af))
            .arg(format!("--demuxer-rawaudio-rate={}", rate))
            .arg("-")
            .stdin(Stdio::piped()).stdout(Stdio::null()).stderr(Stdio::null());
        cmd.spawn().map(|child| AudioOut { child })
    }
}

impl Write for AudioOut {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.child.stdin.as_mut().unwrap().write(buf)
    }
    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl Drop for AudioOut {
    fn drop(&mut self) {
        self.child.stdin.take();
        self.child.kill().ok(); // if for some reason closing the pipe didn't kill it
        self.child.wait().ok(); // should we do this? idk
    }
}
