mpv-audio
=========

mpv-audio is a Rust crate that provides a simple, cross-platform streaming audio output API, powered by mpv.

It Just Works™!

## Usage

```rust
extern crate mpv_audio;

use mpv_audio::*;
use std::io::prelude::*;

fn main() {
    let mut stream = AudioOut::open(AudioFormat::S8, 8000, 1).expect("Couldn't open audio output");
    let mut array = [0u8; 128];
    let mut f: f32 = 0.0;
    loop {
        {
            let mut writer: &mut [u8] = &mut array;
            while !writer.is_empty() {
                writer.write(&[(f.sin()*127.5-0.5) as i8 as u8]).ok();
                f += 0.125; // 0b0.001
                if f >= std::f32::consts::PI*4. {
                    f -= std::f32::consts::PI*4.;
                }
            }
        }
        stream.write_all(&array).expect("couldn't write");
    }
}
```
